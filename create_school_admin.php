<!doctype html>
<html>
<head>
    <title>Create_School_admin</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	    
    <meta name="viewport" content="width=device-width, initial-scale=1" />
   <!-- Latest compiled and minified CSS -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<style>

h3{
	
	margin-left:250px;
}
   .sign {
	border:1px solid black;
	margin-top:35px;
	padding:10px;
	padding-top:20px;
	height:500px;
	max-width:500px;
}

.btn{
	
	margin-left:220px;
}

</style>
</head>

  <body>
<?php
  $firstname=$lastname=$school=$username=$password=$email="";
  $firstnameErr=$lastnameErr=$schoolErr=$usernameErr=$passwordErr=$emailErr="";
  
   if($_SERVER["REQUEST_METHOD"]=="POST"){
	   
	   if(empty($_POST["firstname"])){
		   $firstnameErr="You can't leave this empty";
	   }else{
		   $firstname=$_POST["firstname"];
	   }
	   
	   if(empty($_POST["lastname"])){
		   $lastnameErr="You can't leave this empty";
	   }else{
		   $lastname=$_POST["lastname"];
	   }
	   
	   if(empty($_POST["school"])){
		   $schoolErr="You can't leave this empty";
	   }else{
		   $school=$_POST["school"];
	   }
	   
	   if(empty($_POST["username"])){
		   $usernameErr="You can't leave this empty";
	   }else{
		   $username=$_POST["username"];
	   }
	   
	   if(empty($_POST["password"])){
		   $passwordErr="You can't leave this empty";
	   }else{
		   $password=$_POST["password"];
	   }
	   
	   if(empty($_POST["email"])){
		   $emailErr="You can't leave this empty";
	   }else{
		   $email=$_POST["email"];
		   if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$emailErr= "Invalid email address";
	}
	   }
	   
	   
	   
	   if($firstnameErr=="" AND $lastnameErr=="" AND $schoolErr=="" AND $usernameErr=="" AND
	   $passwordErr=="" AND $emailErr==""){
		   $hpassword=SHA1($password);
		   $dbconnection=mysqli_connect("127.0.0.1","root","","mydb");
		  
		   $query="INSERT INTO `users` (`rollType`,`firstname`,`lastname`,`school_name`,`username`,`password`,`email`) VALUES ('Schooladmin','$firstname','$lastname','$school','$username','$hpassword','$email')";
		   $query1="SELECT LAST_INSERT_ID()";
		  $query2="INSERT INTO `school` (`school_name`) VALUES ('$school')";
		  $query3="SELECT LAST_INSERT_ID()";
		   //$query2="SELECT school.id,users.id FROM `school` INNER JOIN `users` ON school.school_name=users.school_name"; 
		  if(mysqli_query($dbconnection,$query)){
			 echo "School admin created successfully!";
		  }else{
			  echo "Error:".$query."<br>".mysqli_error($dbconnection);
		  };
		  
		  $result1=mysqli_query($dbconnection,$query1);
		   $row1=mysqli_fetch_array($result1);
		   
		   mysqli_query($dbconnection,$query2);
		   $result=mysqli_query($dbconnection,$query3);
		   $row=mysqli_fetch_array($result);
		   
		  if(mysqli_num_rows($result1)>0 AND mysqli_num_rows($result)>0){
			  $query4="INSERT INTO `schoolusers` (`user_id`,`school_id`) VALUES ('$row1[0]','$row[0]')";
			  mysqli_query($dbconnection,$query4);
		  }
		  
		  mysqli_close($dbconnection);
		  header('Location:successSchoolAdmin.php');
		  
	   } 
	   
   }
   
?>
<?php include "pitscoadmin.php"; ?>
  
 
  <h3>Create School admin</h3>
  
  <div class="container sign">
       <form method="POST" class="form-horizontal" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);  ?>">

  
  
 <div class="form-group">
   <label for="firstname" class="col-md-4 control-label">First name</a></label>
     <div class="col-md-8">
      <input type="text" name="firstname" autocomplete="off" value = "<?php echo $firstname; ?>" class="form-control"/> 
	  <span style="color:red"><?php echo $firstnameErr; ?></span>
	  </div>
 </div>
 
 <div class="form-group">
   <label for="lastname" class="col-md-4 control-label">Last name</a></label>
     <div class="col-md-8">
      <input type="text" name="lastname" autocomplete="off" value = "<?php echo $lastname; ?>" class="form-control"/> 
	  <span style="color:red"><?php echo $lastnameErr; ?></span>
	  </div>
 </div>
 
 <div class="form-group">
   <label for="school" class="col-md-4 control-label">School</a></label>
     <div class="col-md-8">
      <input type="text" name="school" autocomplete="off" value = "<?php echo $school; ?>" class="form-control"/> 
	  <span style="color:red"><?php echo $schoolErr; ?></span>
	  </div>
 </div>
 
 <div class="form-group">
   <label for="username" class="col-md-4 control-label">Username</a></label>
     <div class="col-md-8">
      <input type="text" name="username" autocomplete="off" value = "<?php echo $username; ?>" class="form-control"/> 
	  <span style="color:red"><?php echo $usernameErr; ?></span>
	  </div>
 </div>
 
 <div class="form-group">
   <label for="password" class="col-md-4 control-label">Password</a></label>
     <div class="col-md-8">
      <input type="password" name="password" autocomplete="off" value = "<?php echo $password; ?>" class="form-control"/> 
	  <span style="color:red"><?php echo $passwordErr; ?></span>
	  </div>
 </div>
 
 <div class="form-group">
   <label for="email" class="col-md-4 control-label">E-mail</a></label>
     <div class="col-md-8">
      <input type="text" name="email" autocomplete="off" value = "<?php echo $email; ?>" class="form-control"/> 
	  <span style="color:red"><?php echo $emailErr; ?></span>
	  </div>
 </div>
      
	  <button class="btn btn-default type='submit'">Create</button>
	  

    </div> <!-- /container -->
	
	
  </body>
  
</html>  